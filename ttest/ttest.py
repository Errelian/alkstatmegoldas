from math import sqrt
import scipy.stats

def Solve(x,p):
    i = 0
    sumA = 0
    mu0 = x[0]
    while i < len(p):
        sumA = sumA + p[i]
        i = i + 1

    tempAtlag = sumA / len(p)
    j = 0
    sigma = 0

    while j < len(p):
        sigma = sigma + (p[j] - tempAtlag)**2
        j = j + 1

    sigma = sqrt(sigma/len(p))
    t = (tempAtlag - mu0)/(sigma/sqrt(len(p)))
    #print("t:", t)

    if (x[2] == -1.0):
        cv = scipy.stats.t.ppf(x[1], len(p) - 1)
        #print("cv:", cv)
        if t <= cv:
            print(1.0)
        else:
            print(0.0)
    elif (x[2] == 1.0):
        cv =  scipy.stats.t.ppf(1 - x[1], len(p) - 1)
        #print("cv:", cv)
        if cv <= t:
            print(1.0)
        else:
            print(0.0)
    elif (x[2] == 0.0):
        cv =  scipy.stats.t.ppf(1 - x[1]/2, len(p) - 1)
        #print("cv:", cv)
        if (t < -1 * cv) or (cv < t):
            print(1.0)
        else:
            print(0.0)
        
x = input()
x = x.split(' ')
x = [float(i) for i in x]
#print(x)

p = input()
p = p.split(' ')
p = [float(i) for i in p]
#print(p)

Solve(x, p)