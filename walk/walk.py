import operator as op
from functools import reduce
from math import floor

def ncr(n, r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer // denom

def Solve(M):
    if (M == 0):
        print(int(1))
    else:
        if (M % 2 == 1):
            print(int(0))
        else:
            print(ncr(M, floor(M/2)) / (2 ** M))

M=int(input())
Solve(M)