def Solve(ln, vect1, vect2):
    all = 0
    positive = 0
    x = 0
    while x < ln[0]:
        y = 0
        while y < ln[1]:
            if vect1[x] * vect2[y] > 0:

                positive = positive +1
                all = all + 1
            else:

                all = all + 1
            y = y + 1
        x = x + 1

    return str(positive/all)

M1 = input()
ln = M1.split(' ')
ln = [int(i) for i in ln ]

M2 = input()
vect1 = M2.split(' ')
vect1 = [int(i) for i in vect1]

M3 = input()
vect2 = M3.split(' ')
vect2 = [int(i) for i in vect2]

print(Solve(ln, vect1, vect2))