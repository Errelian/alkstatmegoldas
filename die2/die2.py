import numpy as np
import math

import operator as op
from functools import reduce

def ncr(n, r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer // denom

def solve(M):
    Min = M
    current = M
    Max = 6 * M
    while True:
        chance =sum((-1) ** i * ncr(M,i) * ncr(current - 1 - (i*6), M-1) for i in range(0, (math.floor((current-M)/6)+1)))/6 ** M
        print(chance)
        current = current +1
        if(current == Max+1):
            break
M=int(input())
solve(M)