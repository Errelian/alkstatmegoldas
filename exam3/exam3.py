from math import ceil, sqrt

def Solve(n, p):
        return ceil(-1/2*sqrt(-4*n*n*(p - 1) + 4*n*(p - 1) + 1) + n - 1/2)
M = input()
y = M.split(' ')
n = int(y[0])
p = float(y[1])
print(Solve(n, p))