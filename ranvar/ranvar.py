import statistics 
from math import sqrt

def Solve(v,p):
    expected = 0
    i = 0
    while i < len(v):
        expected = expected  + (v[i] * p[i])
        i = i + 1
    std = sqrt(sum( p[j] * (( v[j] - expected ) ** 2) for j in range(0, len(v)) ))
    print(expected)
    print(std)

v = input()
v = v.split(' ')
v = [int(i) for i in v]

p = input()
p = p.split(' ')
p = [float(i) for i in p]

Solve(v,p)
