import numpy as np
def solve(M):
    if M == 0.0:
        return 1
    if M == 1.0:
        return 366
    i = 0
    pa = 1
    while True:
        pa *= (365-i)/365
        pa1 = 1-pa
        if pa1 > M:
            return i+1
        else:
            i = i + 1
M=float(input())
print(solve(M))