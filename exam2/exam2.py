def Solve(n, k):
    return ((n-k)/n)*((n-k-1)/(n-1))*1 + ((n-k)/n)*(k/(n-1))*2*3.5 + (k/n)*((k-1)/(n-1))*3.5
M = input()
y = M.split(' ')
n = int(y[0])
k = int(y[1])
print(Solve(n, k))