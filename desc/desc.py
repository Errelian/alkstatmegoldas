from math import sqrt
import statistics

def Solve(x):
    i = 0
    sumA = 0
    while i < len(x):
        sumA = sumA + x[i]
        i = i + 1
    tempAtlag = sumA / len(x)
    print(tempAtlag)
    korrSzoras = 0
    j = 0
    while j < len(x):
        korrSzoras = korrSzoras + (x[j] - tempAtlag) ** 2
        j = j + 1 
    korrSzoras = korrSzoras / (len(x)-1)
    korrSzoras = sqrt(korrSzoras)
    print(korrSzoras)
    medianV = statistics.median(x)
    print(medianV)
x = input()
x = x.split(' ')
x = [float(i) for i in x]

Solve(x)