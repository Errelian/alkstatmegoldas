def Solve(x, p):
    i = 0
    while i < len(p):
        minCount = 0
        j = 0
        while j < len(x):
            if (x[j] < p[i]):
                minCount = minCount + 1
            j = j + 1
        print(float(minCount/len(x)))
        i = i + 1
length = input()
length = length.split(' ')
length = [int(i) for i in length]

x = input()
x = x.split(' ')
x = [float(i) for i in x]

p = input()
p = p.split(' ')
p = [float(i) for i in p]

Solve(x, p)