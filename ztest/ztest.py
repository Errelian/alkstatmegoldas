import scipy.stats
from math import sqrt

def Solve(x, p):
    i = 0
    sumA = 0
    mu0 = x[0]
    sigma = x[1]
    while i < len(p):
        sumA = sumA + p[i]
        i = i + 1
    tempAtlag = sumA / len(p)
    #print("tempAtlag:", tempAtlag)
    z = (tempAtlag - mu0) * (sqrt(len(p))/sigma)
    #print("z:",z)
    if (x[3] == -1.0):
        cv = scipy.stats.norm.ppf(x[2])
        if z <= cv:
            print(1.0)
        else:
            print(0.0)
    elif (x[3] == 1.0):
        cv =  scipy.stats.norm.ppf(1 - x[2])
        if cv <= z:
            print(1.0)
        else:
            print(0.0)
    elif (x[3] == 0.0):
        cv =  scipy.stats.norm.ppf(1 - x[2]/2)
        #print("cv:", cv)
        if (z < -1 * cv) or (cv < z):
            print(1.0)
        else:
            print(0.0)
x = input()
x = x.split(' ')
x = [float(i) for i in x]
#print(x)

p = input()
p = p.split(' ')
p = [float(i) for i in p]
#print(p)

Solve(x,p)