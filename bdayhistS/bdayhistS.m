M = 50;
ISM = 64;
ranvarVector = zeros(1, ISM); %a random varieble-ket tartalmazó vektor

for i=1:ISM
    birthDates = randi(365, [1, M]);%születési dátumok legenerálása
    sort(birthDates); %sorba rendezés mert így sokkal gyorsabb checkelni
    x = 1;
    currentMatch = 1; %a mostani sorozatban hány egyező van
    for j=1:birthDates
        if j < length(birthDates)
            if birthDates(j) == birthDates(j+1)
                currentMatch = currentMatch  + 1;
            else
                currentMatch = 1;
            end
        end
        if currentMatch > x
            x = currentMatch;
        end
    end
    ranvarVector(i) = x;
end

relDist = zeros(1, 10); %preallokálok hogy szebb legyen
ranvarVector;
for i=1:length(ranvarVector)
    relDist(ranvarVector(i)) = relDist(ranvarVector(i)) + 1;
end

for i=1:length(relDist)
    relDist(i) = relDist(i)/ISM;
end

bar(relDist)
xlim([1 10])
set(gca, 'xtick', y) % lehetne szebb is a megjelenítés, de a lényeg látszik