M = 50;
ISM = 64;
K = 2;

ranvarVector = zeros(1, ISM); %a random varieble-ket tartalmazó vektor

for i=1:ISM
    birthDates = randi(365, [1, M]);%születési dátumok legenerálása
    sort(birthDates); %sorba rendezés mert így sokkal gyorsabb checkelni
    x = 1;
    currentMatch = 1; %a mostani sorozatban hány egyező van
    for j=1:birthDates
        if j < length(birthDates)
            if birthDates(j) == birthDates(j+1)
                currentMatch = currentMatch  + 1;
            else
                currentMatch = 1;
            end
        end
        if currentMatch > x
            x = currentMatch;
        end
    end
    ranvarVector(i) = x;
end

boolVar = zeros(1, ISM);

for i=1:ISM
    if K <= ranvarVector(i)
        boolVar(i) = boolVar(i) + 1;
    end
    if i ~= 1
        boolVar(i) = boolVar(i) + boolVar(i-1)
    end
end

relFreq = zeros(1, ISM);

for i=1:ISM
    relFreq(i) = boolVar(i) / i;
end

plot(relFreq)
xlim([0 ISM]);
ylim([0.0 1.0]);